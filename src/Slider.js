var mySlider = function(obj, elem) {
	if(typeof obj === "number" || typeof obj === "string") {
		throw new Error("wrong datatype");
	}

	$(elem).css({'left': obj.x, 'top': obj.y});
}