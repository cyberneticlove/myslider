// mySlider should
// 1. have a move function that accepts an object argument
// 2. The argument should also have an X coordinate and a Y coordinate
// 3. mySlider should move an element to the position from the argument coordinates


describe("mySlider", function () {
	it("should be a function", function() {
		expect(mySlider).toBeDefined();
		expect(typeof mySlider).toEqual("function");
	})

	it("should accept an argument that is an object", function() {
		$('<div id="square-point">').appendTo('body');

		var myArgument = { 
			x: '252px',
			y: '112px'
		};

		var myElement = $('#square-point');

		expect(function(){
			mySlider(myArgument, myElement);
		}).not.toThrow();

		expect(function(){
			mySlider(121, myElement);
		}).toThrow();

		expect(function() {
			mySlider('121px', myElement);
		}).toThrow();
		
		$('#square-point').remove();
	})

	it("should accept an argument that is an element", function() {
		expect(function() {
			mySlider($('body')).not.toThrow();
		})
	});


	describe("animation", function() {
		beforeEach(function() {
			$('<div id="square-point">').appendTo('body');
		})

		afterEach(function() {
			$('#square-point').remove();
		})

		it("moves the element according to argument coordinates", function() {
			mySlider({x: '100px', y: '100px'}, $('#square-point'));
			expect(document.getElementById('square-point').style.left).toEqual('100px');
			expect(document.getElementById('square-point').style.top).toEqual('100px');
		})
	})
})